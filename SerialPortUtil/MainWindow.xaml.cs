﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using SerialPortLib;

namespace SerialPortUtil
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SerialPortController sp;

        public MainWindow()
        {
            InitializeComponent();
            sp = new SerialPortController();

            sp.messageEventHandler += WriteLineToConsole;
        }

        private void Connect_Button_Click(object sender, RoutedEventArgs e)
        {
            
            
            // assign variables to instance

            // add event handler to messages

            sp.Open();
        }

        void WriteLineToConsole(string line)
        {
            ListBoxItem item = new ListBoxItem();
            item.Content = line;
            console.Items.Add(item);
        }
    }
}
