﻿using System;
using System.IO.Ports;

namespace SerialPortLib
{
    public enum SerialPortControllerMessageType { 
        Normal, 
        Error 
    }
    public enum SerialPortControllerPortName {
        COM1,
        COM2,
        COM3,
        COM4,
        COM5,
        COM6,
        COM7,
        COM8,
        COM9
    }
    public enum SerialPortControllerBaudRate {
        _110    = 110,
        _300    = 300,
        _600    = 600,
        _1200   = 1200,
        _2400   = 2400,
        _4800   = 4800,
        _9600   = 9600,
        _14400  = 14400,
        _19200  = 19200,
        _28800  = 28800,
        _38400  = 38400,
        _56000  = 56000,
        _57600  = 57600,
        _115200 = 115200
    }
    public delegate void SerialPortControllerMessage(string message);

    public class SerialPortController
    {
        public event SerialPortControllerMessage messageEventHandler;

        public SerialPort serialPort;

        // settings
        string portName     = SerialPortControllerPortName.COM1.ToString();
        int baudRate        = (int)SerialPortControllerBaudRate._9600;
        Parity parity       = Parity.None;
        int dataBits        = 8;
        Handshake handshake = Handshake.None;

        public SerialPortController()
        {
            serialPort = new SerialPort(portName);

            serialPort.BaudRate  = baudRate;
            serialPort.Parity    = parity;
            serialPort.StopBits  = StopBits.One;
            serialPort.DataBits  = dataBits;
            serialPort.Handshake = handshake;
        }
        public void Open()
        {
            try
            {
                serialPort.Open();
            }
            catch (Exception e)
            {
                string message = "Error: " + e.Message;
                SendMessage(message);
            }
        }
        public void Close() 
        {
            serialPort.Close();
        }
        public void SendMessage(string message) {
            if (messageEventHandler != null)
                messageEventHandler(message);
        }
        // Summary:
        //     The Clear to Send (CTS) signal changed state. This signal is used to indicate
        //     whether data can be sent over the serial port.
        //CtsChanged = 8,
        //
        // Summary:
        //     The Data Set Ready (DSR) signal changed state. This signal is used to indicate
        //     whether the device on the serial port is ready to operate.
        //DsrChanged = 16,
        //
        // Summary:
        //     The Carrier Detect (CD) signal changed state. This signal is used to indicate
        //     whether a modem is connected to a working phone line and a data carrier signal
        //     is detected.
        //CDChanged = 32,
        //
        // Summary:
        //     A break was detected on input.
        //Break = 64,
        //
        // Summary:
        //     A ring indicator was detected.
        //Ring = 256,
        void PinChangedConsoleDumpHandler(object sender, SerialPinChangedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;

            SendMessage(DateTime.Now.ToString("yyyyMMddHHmmssffff") + ":PinChangeEvent: " + e.EventType.ToString());
            SendMessage("CtsChanged: " + sp.CtsHolding.ToString());
            SendMessage("DsrChanged: " + sp.DsrHolding.ToString());
            SendMessage("CDChanged: " + sp.CDHolding.ToString());
            SendMessage("Break: " + sp.BreakState.ToString());
        }

        void DataReceivedConsoleDumpHandler(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            string indata = sp.ReadExisting();
            SendMessage("Data Received:");
            SendMessage(indata);
        }
    }
}
