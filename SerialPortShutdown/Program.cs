﻿using System;
using System.IO.Ports;

class PortDataReceived
{
    public static void Main()
    {
        SerialPort sp = new SerialPort("COM1");

        sp.BaudRate  = 9600;
        sp.Parity    = Parity.None;
        sp.StopBits  = StopBits.One;
        sp.DataBits  = 8;
        sp.Handshake = Handshake.None;

        sp.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
        sp.PinChanged   += sp_PinChanged;
        
        try
        {
            sp.Open();
        }
        catch (Exception e)
        {
            Console.WriteLine("Error: " + e.Message);
        }

        Console.WriteLine("Press any key to continue...");
        Console.WriteLine();
        Console.ReadKey();
        sp.Close();
    }

    static void sp_PinChanged(object sender, SerialPinChangedEventArgs e)
    {
        SerialPort sp = (SerialPort)sender;

        Console.WriteLine(DateTime.Now.ToString("yyyyMMddHHmmssffff") + ":PinChangeEvent: " + e.EventType.ToString());
        Console.WriteLine("CtsChanged: " + sp.CtsHolding.ToString());
        Console.WriteLine("DsrChanged: " + sp.DsrHolding.ToString());
        Console.WriteLine("CDChanged: " + sp.CDHolding.ToString());
        Console.WriteLine("Break: " + sp.BreakState.ToString());
    }

    private static void DataReceivedHandler(
                        object sender,
                        SerialDataReceivedEventArgs e)
    {
        SerialPort sp = (SerialPort)sender;
        string indata = sp.ReadExisting();
        Console.WriteLine("Data Received:");
        Console.Write(indata);
    }
}